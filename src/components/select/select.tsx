import React, { useEffect, useState, useRef } from 'react';

import { useClickOutside } from '@/hooks';
import { Placeholder, SelectItem } from '@/components/atoms';

import styles from './select.module.scss';

interface SelectProps {
  handleSelect: (item: string) => void;
  items: string[];
  selectedItems: string[];
}

export const Select = ({ handleSelect, items, selectedItems }: SelectProps) => {
  const ref = useRef<HTMLDivElement>(null);

  const [searchQuery, setSearchQuery] = useState('');
  const [expanded, setExpanded] = useState(false);

  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value);
  };

  const closeSelect = () => setExpanded(false);

  useClickOutside(ref, closeSelect);

  useEffect(() => {
    setSearchQuery('');
  }, [expanded]);

  const filteredItems = items.filter((item) => {
    if (!searchQuery) {
      return true;
    }
    if (item.toLowerCase().includes(searchQuery.toLowerCase())) {
      return true;
    }
    return false;
  });

  return (
    <div ref={ref} className={styles.select} onClick={() => setExpanded(true)}>
      {expanded ? (
        <>
          <input
            autoFocus
            className={styles.input}
            type='text'
            value={searchQuery}
            onChange={handleSearch}
          />
          <div className={styles.search}>
            {filteredItems.length > 0 ? (
              filteredItems.map((item) => (
                <SelectItem
                  checked={
                    selectedItems.length > 0
                      ? selectedItems.includes(item)
                      : false
                  }
                  item={item}
                  key={item}
                  handleClick={() => handleSelect(item)}
                />
              ))
            ) : (
              <p>nothing to show</p>
            )}
          </div>
        </>
      ) : (
        <Placeholder selectedItems={selectedItems} />
      )}
    </div>
  );
};
