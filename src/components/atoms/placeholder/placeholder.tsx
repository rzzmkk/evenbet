import React from 'react';

import styles from './placeholder.module.scss';

interface PlaceholderProps {
  selectedItems: string[];
}

export const Placeholder = ({ selectedItems }: PlaceholderProps) => {
  const selectedList = selectedItems.sort().join(', ');
  return (
    <div className={styles.placeholder}>
      {selectedItems.length > 0 ? <p>{selectedList}</p> : <p>Please Select</p>}
    </div>
  );
};
