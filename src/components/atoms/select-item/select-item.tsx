import React from 'react';

import styles from './select-item.module.scss';

interface SelectItemProps {
  checked: boolean;
  item: string;
  handleClick: (item: string) => void;
}

export const SelectItem = ({ checked, item, handleClick }: SelectItemProps) => (
  <div className={styles.item} onClick={() => handleClick(item)}>
    <input type='checkbox' checked={checked} onChange={() => null} />
    <p>{item}</p>
  </div>
);
