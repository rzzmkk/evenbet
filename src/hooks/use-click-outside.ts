import { useEffect, useCallback } from 'react';
import type { RefObject } from 'react';

export function useClickOutside<T extends HTMLElement>(
  ref: RefObject<T>,
  callback?: () => void
) {
  const handleBodyClick = useCallback(
    (event: MouseEvent) => {
      if (ref.current && !ref.current.contains(event.target as HTMLElement)) {
        callback?.();
      }
    },
    [callback]
  );

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      document.addEventListener('click', handleBodyClick);
    });

    return () => {
      clearTimeout(timeoutId);
      document.removeEventListener('click', handleBodyClick);
    };
  }, [handleBodyClick]);
}
